# GENEPY3D

GeNePy3D is an open-source python library for quantitative geometry, in particular in the context of quantitative microscopy. 
It provides an easy interface to perform complex data science workflow on geometric data, 
manipulating point cloud, surfaces, curves and trees, converting between them, 
and performing analysis tasks to extract information and knowledge out of raw geometrical data.

GeNePy3D can be used as a 'middleware' library, that is linking out to recognised and established specialised library when needed but providing easy to use interfaces. 
The aim is to democratise access to methods from a wide range of mathematical sub-discipline to biological data scientist and computational biologist.

Further information can be found in the main documentation https://genepy3d.gitlab.io/.

Reference documentation is on readthedocs https://genepy3d.readthedocs.io/




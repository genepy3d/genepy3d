genepy3d.objext package
=======================

Module contents
---------------

.. automodule:: genepy3d.objext
   :members:
   :undoc-members:
   :show-inheritance:

Submodules
----------

genepy3d.objext.simpletracks module
-----------------------------------

.. automodule:: genepy3d.objext.simpletracks
   :members:
   :autosummary:
   :undoc-members:
   :show-inheritance:




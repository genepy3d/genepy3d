genepy3d.obj package
====================

Module contents
---------------

.. automodule:: genepy3d.obj
   :members:
   :undoc-members:
   :show-inheritance:

Submodules
----------

genepy3d.obj.curves module
--------------------------

.. automodule:: genepy3d.obj.curves
   :members:
   :autosummary:
   :undoc-members:
   :show-inheritance:

genepy3d.obj.points module
--------------------------

.. automodule:: genepy3d.obj.points
   :members:
   :autosummary:
   :undoc-members:
   :show-inheritance:

genepy3d.obj.surfaces module
----------------------------

.. automodule:: genepy3d.obj.surfaces
   :members:
   :autosummary:
   :undoc-members:
   :show-inheritance:

genepy3d.obj.trees module
-------------------------

.. automodule:: genepy3d.obj.trees
   :members:
   :autosummary:
   :undoc-members:
   :show-inheritance:



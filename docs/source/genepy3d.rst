genepy3d package
================

Module contents
---------------

.. automodule:: genepy3d
   :members:
   :undoc-members:
   :show-inheritance:

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   genepy3d.io
   genepy3d.obj
   genepy3d.objext
   genepy3d.util

genepy3d.io package
===================

Module contents
---------------

.. automodule:: genepy3d.io
   :members:
   :undoc-members:
   :show-inheritance:

Submodules
----------

genepy3d.io.base module
-----------------------

.. automodule:: genepy3d.io.base
   :members:
   :autosummary:
   :undoc-members:
   :show-inheritance:

genepy3d.io.catmaid module
--------------------------

.. automodule:: genepy3d.io.catmaid
   :members:
   :autosummary:
   :undoc-members:
   :show-inheritance:

genepy3d.io.swc module
----------------------

.. automodule:: genepy3d.io.swc
   :members:
   :autosummary:
   :undoc-members:
   :show-inheritance:

genepy3d.io.trackmate module
----------------------------

.. automodule:: genepy3d.io.trackmate
   :members:
   :autosummary:
   :undoc-members:
   :show-inheritance:



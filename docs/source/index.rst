.. genepy3d documentation master file, created by
   sphinx-quickstart on Wed Sep 18 15:44:48 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

GeNePy3D documentation
======================

You find here the reference documentation of GeNePy3D, a python library for quantitative geometry. 
GeNePy3D is released under the BSD 3-Clause license. However, there are some functions that we used packages under the GPL license, in particular CGAL. 
These functions are implemented in a distinct package `GeNePy3D_GPL <https://gitlab.com/genepy3d/genepy3d_gpl>`_.

For the installation, tutorial and application, please check its main page: https://genepy3d.gitlab.io.

The GeNePy3D source code is found here: https://gitlab.com/genepy3d/genepy3d.

.. toctree::
   :maxdepth: 4

   genepy3d

Funding
=======

This development was originaly supported by Agence Nationale de la Recherche (contract ANR-11-EQPX-0029 Morphoscope2).   


.. Indices and tables
.. ==================

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`
